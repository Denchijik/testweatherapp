import React from 'react';
import { useDispatch } from 'react-redux'
import { performSearch } from '../CitySearch/duck'
import LastSearchItem from './LastSearchItem/index'
import './styles.css'

const LastSearches = React.memo((props) => {
    const dispatch = useDispatch();

    const setSearch = (cityName) => {
        dispatch(performSearch({cityName}));
    }

    return (
        <>
            <h4 className="last-searches-title">Last searches:</h4>
            {
                props.lastSearches.map((cityName, i) =>
                    <LastSearchItem key={i} handleItemClick={setSearch} cityName={cityName}></LastSearchItem>
                )
            }
        </>
    );
});

export default LastSearches;