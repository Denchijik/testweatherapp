import Enumerable from 'linq'

export const convertAPIResultToWeatherData = (fetchResult) => {
    const weekWeatherList = Enumerable.from(fetchResult.list)
        .groupBy(item => new Date(item.dt_txt).toDateString(), null , (key, group) => {
            return {
                date: key,
                sky: group.first().weather[0].main,
                maxTemp: Math.round(group.max(item => item.main.temp)),
                minTemp: Math.round(group.min(item => item.main.temp)),
                wind: Math.round(group.min(item => item.wind.speed)) + "-" + Math.round(group.max(item => item.wind.speed))
            }
        })
        .toArray();

    const firstWeatherItem = fetchResult.list[0];
    const weatherNow = {
        temperature: Math.round(firstWeatherItem.main.temp),
        humidity: firstWeatherItem.main.humidity,
        wind: Math.round(firstWeatherItem.wind.speed),
        sky: firstWeatherItem.weather[0].main
    };

    const weatherPeriods = Enumerable.from(fetchResult.list)
        .take(5)
        .toArray()
        .map((item) => {
            return {
                timeText: new Date(item.dt_txt).toLocaleTimeString().slice(0,-3),
                temperature: Math.round(item.main.temp),
                sky: item.weather[0].main
            }
        });
    

    const weatherData = {
        cityName: fetchResult.city.name,
        currentDateText: 1,
        weatherNow,
        weatherPeriods,
        weatherList: weekWeatherList
    }

    console.warn(weatherData);

    return weatherData;
}