import React from 'react';
import WeatherPeriod from './WeatherPeriod'
import { getWeatherIcon } from '../../../../helpers/weatherDataHelper'
import './styles.css'
import '../styles.css'

const WeatherDetail= (props) => {
    const { cityName, weatherDetail, weatherPeriods } = props;

    return (
        <div className="weather-box-container">
            <div className="weather-box weather-detail-box">
                <div className="weather-box-title-container">
                    <span className="weather-detail-cityText">{cityName}, </span>
                    <span className="weather-detail-dateText">{new Date().toDateString()}</span>
                </div>
                <div className="weather-box-detail-container">
                    <div className="weather-box-detail-image-container">
                        <img src={getWeatherIcon(weatherDetail.sky)} alt="" className="weather-now-image" ></img>
                    </div>
                    <div className="weather-box-detail-text-container">
                        <p>Temperature: {weatherDetail.temperature}°C</p>
                        <p>Wind: {weatherDetail.wind} m/s</p>
                        <p>Humidity: {weatherDetail.humidity}%</p>
                    </div>
                </div>
                <div className="weather-box-period-container">
                    {
                        weatherPeriods.map((weatherItem, i) => (
                            <WeatherPeriod key={i} {...weatherItem} />
                        ))
                    }
                </div>
            </div>
        </div>
    );
}

export default WeatherDetail;