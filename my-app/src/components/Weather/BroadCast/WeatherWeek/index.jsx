import React from 'react';
import WeatherDay from './WeatherDay'

import './styles.css'
const WeatherWeek = (props) => {

    return (
        <div className="weather-week-container">
            {
                props.weatherList.map((weatherItem, i) =>
                    <WeatherDay key={i} {...weatherItem} />
                )
            }
        </div>
    );
}

export default WeatherWeek