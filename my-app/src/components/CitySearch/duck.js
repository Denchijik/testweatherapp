const initialState = {
    currentSearch: ''
};

export const searchReducer = (state = initialState, action) => {
    if (action.type === "PERFORM_SEARCH") {
        return {...state, currentSearch: action.payload}
    }
    return state;
}

export const performSearch = (payload) => {
    return { type: "PERFORM_SEARCH", payload }
};