import React from 'react';
import { getWeatherIcon } from '../../../../../helpers/weatherDataHelper'
import './styles.css'

const WeatherDay = (props)=> {
    const { date, sky, minTemp, maxTemp, wind } = props;
    return (
        <div className="weather-box weather-day-box">
            <div>
                <span className="weather-day-title">{date}</span>
            </div>
            <div>
                <img src={getWeatherIcon(sky)} alt="" className="weather-day-image"></img>
            </div>
            <div className="weather-day-text">
                <p>Min temp: {minTemp}°C</p>
                <p>Max temp: {maxTemp}°C</p>
                <p>Wind: {wind} m/s</p>
            </div>
        </div>
    );
}

export default WeatherDay;