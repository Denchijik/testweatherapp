import React from 'react'

const LastSearchItem = (props) => {
    let handleClick = (event, cityName) => {
        event.preventDefault();
        props.handleItemClick(cityName);
    }

    return (
        <>
            <p><a href="" onClick={e => handleClick(e, props.cityName)}>{props.cityName}</a></p>
        </>
    );
}

export default LastSearchItem;