export const updateLastSearchesInLocalStorage = (cityName) => {
    let lastSearches = [];
    if(!cityName || cityName === "City Not Found"){
        return lastSearches
    }
    let lastSearchesString = localStorage.getItem("lastSearch");
    if (lastSearchesString) {
        lastSearches = JSON.parse(lastSearchesString);
    }
    if (lastSearches.indexOf(cityName) !== -1)
        return lastSearches;
    if (lastSearches.length > 2) {
        lastSearches.pop();
    }
    lastSearches.unshift(cityName);
    localStorage.setItem("lastSearch", JSON.stringify(lastSearches));
    return lastSearches;
};