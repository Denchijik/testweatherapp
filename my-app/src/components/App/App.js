import React from 'react';
import { createStore } from "redux";
import { Provider } from 'react-redux';
import './App.css';
import Weather from '../Weather/index'
import rootReducer from "../../store/rootReducer";

const store = createStore(rootReducer);

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Weather />
      </div>
    </Provider>
  );
}

export default App;
