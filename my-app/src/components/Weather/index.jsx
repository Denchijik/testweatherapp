import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux'

import CitySearch from '../CitySearch'
import BroadCast from './BroadCast'
import LastSearches from '../LastSearches'
import { convertAPIResultToWeatherData } from '../../helpers/weatherDataConverter'
import { updateLastSearchesInLocalStorage } from '../../helpers/localStorageHelper'
import * as SearchService from '../CitySearch//SearchService'
import { currentSearchSelector } from '../../selectors/searchSelector'

import './styles.css'
//ToDo: make weather of month and full info of 1 day
const Weather = () => {

    const [weatherData, setWeatherData] = useState({});
    const [lastSearches, setLastSearches] = useState([]);

    const currentSearch = useSelector(currentSearchSelector);

    useEffect(() => {
        let lastSearchesString = localStorage.getItem("lastSearch");
        if (lastSearchesString) {
            setLastSearches(JSON.parse(lastSearchesString));
        }
    }, []);

    const getWeatherData = useCallback((cityName) => {
        if (!cityName)
            return;

        SearchService.getWeatherData(cityName)
            .then(response => response.json())
            .then(result => {
                let newWeatherData = {
                    cityName: "City Not Found",
                }
                if (result.cod === "200" && result.list.length > 0) {
                    newWeatherData = convertAPIResultToWeatherData(result);          
                }
                setWeatherData(newWeatherData);
                if (lastSearches.indexOf(cityName) === -1) {
                    setLastSearches(updateLastSearchesInLocalStorage(cityName));
                }
            })
            .catch(err => {
                console.error(err);
            });
    },[lastSearches]);

    useEffect(()=>{
        getWeatherData(currentSearch.cityName);
    }, [currentSearch, getWeatherData])

    return (
        <>
            <h1>Weather broadcast</h1>
            <div className="content-container">
                <CitySearch />
                <BroadCast weatherData={weatherData} />
                <LastSearches lastSearches={lastSearches} />
            </div>
        </>
    );
};

export default Weather;