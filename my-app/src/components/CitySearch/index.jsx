import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { performSearch } from './duck';

import './styles.css';

const CitySearch = () => {
    const [cityName, setCityName] = useState('');
    const dispatch = useDispatch();

    const handleClick = (event) => {
        event.preventDefault();
        dispatch(performSearch({cityName}));    
    }

    return (
        <div className="searchContainer">
            <input type="text" id="test" placeholder="Enter city" className="input"
                value={cityName}
                onChange={event => setCityName(event.target.value)} />
            <button type="button" className="button blue-button"
                onClick={handleClick}>
                View weather
            </button>
        </div>
    );
}

export default CitySearch