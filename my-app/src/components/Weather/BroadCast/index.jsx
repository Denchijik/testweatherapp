import React from 'react'
import WeatherDetail from './WeatherDetail'
import WeatherWeek from './WeatherWeek'

const BroadCast = (props) => {   
    const { weatherData } = props;
    return (
        <>
            {weatherData.weatherNow &&
                <div>
                    <WeatherDetail cityName={weatherData.cityName} weatherDetail={weatherData.weatherNow} weatherPeriods={weatherData.weatherPeriods} />
                    <WeatherWeek weatherList={weatherData.weatherList} />
                </div>
            }
        </>
    );
}
export default BroadCast;