import React from 'react';
import { getWeatherIcon } from '../../../../../helpers/weatherDataHelper'
import './styles.css'

const WeatherDetail= (props) => {
    return (
        <div className="weather-period">
            <div className="timeText">
                <span>{props.timeText}</span>
            </div>
            <div>
                <img src={getWeatherIcon(props.sky)} alt="" className="weather-period-image"></img>
            </div>
            <div className="temperatureText">
                <span>{props.temperature}°C</span>
            </div>
        </div>
    )
}

export default WeatherDetail;