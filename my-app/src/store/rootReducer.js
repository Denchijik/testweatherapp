
import { combineReducers } from 'redux';
import { searchReducer as search } from '../components/CitySearch/duck'

const allReducers = {
    search,
}

export default combineReducers(allReducers);