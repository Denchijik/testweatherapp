
import Cloudy from '../icons/clouds.svg'
import Sunny from '../icons/sun.svg'
import Rain from '../icons/rain.svg'

export const getWeatherIcon = (sky) => {
    switch (sky) {
        case "Clouds":
            return Cloudy;
        case "Clear":
            return Sunny;
        case "Rain":
            return Rain;
        default:
            return "";
    }
}